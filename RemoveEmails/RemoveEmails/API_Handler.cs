﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RemoveEmails
{
    class API_Handler
    {      
        

        const string defaultListIds = "2575706";
        const string defaultNonKeyedListIds = "2884346";
        const string loginEnvelopeFormat = @"<Envelope><Body><Login><USERNAME>{0}</USERNAME><PASSWORD>{1}</PASSWORD></Login></Body></Envelope>";
        const string recipientDataEnvelopeFormat = @"<Envelope><Body><SelectRecipientData><LIST_ID>{0}</LIST_ID><EMAIL>{1}</EMAIL><COLUMN><NAME>Email</NAME><VALUE>{1}</VALUE></COLUMN></SelectRecipientData></Body></Envelope>";
        const string recipientDataNonKeyedEnvelopeFormat = @"<Envelope><Body><SelectRecipientData><LIST_ID>{0}</LIST_ID><COLUMN><NAME>Email</NAME><VALUE>{1}</VALUE></COLUMN></SelectRecipientData></Body></Envelope>";
        const string listRecipientMailingsEnvelopeFormat = @"<Envelope><Body><ListRecipientMailings><LIST_ID>{0}</LIST_ID><RECIPIENT_ID>{1}</RECIPIENT_ID></ListRecipientMailings></Body></Envelope>";

        const string getListsEnvelopeFormat = @"<Envelope><Body><GetLists><VISIBILITY>1</VISIBILITY><LIST_TYPE>0</LIST_TYPE></GetLists></Body></Envelope>";

        const string optOutRecipientEnvelopeFormat = @"<Envelope><Body><OptOutRecipient><LIST_ID>{1}</LIST_ID><EMAIL>{0}</EMAIL></OptOutRecipient></Body></Envelope>";



        public static List<SilverPopListForEmail> Lists(string email, string sessionId = null)
        {
            if (string.IsNullOrEmpty(sessionId)) sessionId = GetSessionId();
            var lists = GetAllSharedSilverPopLists(sessionId).Select(x => new SilverPopListForEmail() { ListId = x.ListId, ListName = x.ListName }).ToList();
            foreach (var list in lists)
            {
                var optedOut = default(DateTime?);
                if (isInList(email, list.ListId, out optedOut, sessionId))
                {
                    list.IsInList = true;
                    if (optedOut != null)
                    {
                        list.OptOutDate = optedOut;
                    }
                }
            }
            return lists;
        }

        private static bool isInList(string email, string listId, out DateTime? optedOut, string sessionId = null)
        {
            if (string.IsNullOrEmpty(sessionId)) sessionId = GetSessionId();
            optedOut = null;
            var document = @"<Envelope><Body><SelectRecipientData><LIST_ID>{1}</LIST_ID><EMAIL>{0}</EMAIL><COLUMN><NAME>Email</NAME><VALUE>{0}</VALUE></COLUMN></SelectRecipientData></Body></Envelope>";
            document = string.Format(document, email, listId);
            var result = default(string);
            result = SilverPopXMLAPI(document, sessionId);//document.HttpPostXML(Definitions.API_URL + session_encoding, out result);
            var selectRecipientDataResult = XDocument.Parse(result);
            var found = selectRecipientDataResult.XPathSelectElements("/Envelope/Body/RESULT/SUCCESS").FirstOrDefault();
            var optedOutNode = selectRecipientDataResult.XPathSelectElements("/Envelope/Body/RESULT/OptedOut").FirstOrDefault();
            if (optedOutNode != null && !string.IsNullOrEmpty(optedOutNode.Value))
            {
                var dt = default(DateTime);
                if (DateTime.TryParse(optedOutNode.Value, out dt))
                {
                    optedOut = dt;
                }
            }
            if (found == null) return false;
            return found.Value.ToLowerInvariant() == "true";
        }

        public static string GetSessionId()
        {
            string username = ConfigurationManager.AppSettings["UserName"].ToString();
            string password = ConfigurationManager.AppSettings["Password"].ToString();

            var loginEnvelope = string.Format(loginEnvelopeFormat, username, password);
            var loginDocument = XDocument.Parse(SilverPopXMLAPI(loginEnvelope, ""));
            var sessionIdNode = loginDocument.Descendants().FirstOrDefault(x => x.Name == "SESSIONID");
            if (sessionIdNode != null)
            {
                return sessionIdNode.Value;
            }
            return null;
        }

        public static List<SilverPopList> GetAllSharedSilverPopLists(string sessionId = null)
        {
            if (sessionId == null) GetSessionId();
            string result;
            result = SilverPopXMLAPI(getListsEnvelopeFormat);
            //document.HttpPostXML(Definitions.API_URL + session_encoding, out result);
            var lists = new List<SilverPopList>();
            var doc = XDocument.Load(new StringReader(result));
            foreach (var element in doc.XPathSelectElements("/Envelope/Body/RESULT/LIST"))
            {
                lists.Add(new SilverPopList() { ListId = element.Element("ID").Value, ListName = element.Element("NAME").Value });
            }
            return lists;
        }

        public static List<SilverPopMailing> EmailHistory(string email, string sessionId = null)
        {
            if (string.IsNullOrEmpty(sessionId)) sessionId = GetSessionId();

            if (!string.IsNullOrEmpty(sessionId))
            {
                var list = new List<SilverPopMailing>();
                foreach (var spList in Lists(email))
                {
                    var defaultListId = spList.ListId;
                    var recipientDataEnvelope = string.Format(recipientDataEnvelopeFormat, defaultListId, email);
                    var recipientDataDocument = XDocument.Parse(SilverPopXMLAPI(recipientDataEnvelope, sessionId));
                    var recipientIdNode = recipientDataDocument.Descendants().FirstOrDefault(x => x.Name == "RecipientId");
                    if (recipientIdNode != null)
                    {
                        var recipientId = recipientIdNode.Value;
                        var recipientMailingsEnvelope = string.Format(listRecipientMailingsEnvelopeFormat, defaultListId, recipientId);
                        var recipientMailingsDocument = XDocument.Parse(SilverPopXMLAPI(recipientMailingsEnvelope, sessionId));
                        foreach (var descendant in recipientMailingsDocument.Descendants("Mailing"))
                        {
                            var mailing = new SilverPopMailing();
                            mailing.ListName = spList.ListName;
                            mailing.OptOutDate = spList.OptOutDate;
                            mailing.MailingId = descendant.Descendants("MailingId").First().Value;
                            mailing.ListId = spList.ListId;
                            mailing.MailingName = descendant.Descendants("MailingName").First().Value;
                            mailing.ReportId = descendant.Descendants("ReportId").First().Value;
                            mailing.SentTS = DateTime.Parse(descendant.Descendants("SentTS").First().Value);
                            mailing.TotalAttachments = descendant.Descendants("TotalAttachments").First().Value;
                            mailing.TotalBounces = descendant.Descendants("TotalBounces").First().Value;
                            mailing.TotalClicks = descendant.Descendants("TotalClicks").First().Value;
                            mailing.TotalClickstreams = descendant.Descendants("TotalClickstreams").First().Value;
                            mailing.TotalConversions = descendant.Descendants("TotalConversions").First().Value;
                            mailing.TotalForwards = descendant.Descendants("TotalForwards").First().Value;
                            mailing.TotalMediaPlays = descendant.Descendants("TotalMediaPlays").First().Value;
                            mailing.TotalOpens = descendant.Descendants("TotalOpens").First().Value;
                            mailing.TotalOptOuts = descendant.Descendants("TotalOptOuts").First().Value;
                            list.Add(mailing);
                        }
                    }
                }


                return list;
            }
            throw new NullReferenceException("Could not get SessionId");
        }

        private static string SilverPopXMLAPI(string loginEnvelope, string sessionId = null)
        {
            if (sessionId == null) sessionId = GetSessionId();

            var responseFromServer = default(string);
            var apiHost = "https://api1.silverpop.com/XMLAPI";
            if (!string.IsNullOrEmpty(sessionId))
            {
                apiHost = string.Format("{0};jsessionid={1}", apiHost, sessionId);
            }
            var request = WebRequest.Create(apiHost);
            request.Method = "POST";
            var byteArray = Encoding.UTF8.GetBytes(loginEnvelope);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = byteArray.Length;
            using (var dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
            }

            using (var response = request.GetResponse())
            {
                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseFromServer = reader.ReadToEnd();
                            reader.Close();
                        }
                        responseStream.Close();
                        response.Close();
                    }
                }
            }
            return responseFromServer;
        }

        public static RemoveFromListResult OptOutOfList(string email, string listId, string sessionId = null)
        {
            var removeFromListResult = new RemoveFromListResult() { ListId = listId };
            var document = string.Format(optOutRecipientEnvelopeFormat, email, listId);
            var result = SilverPopXMLAPI(document);
            //            document.HttpPostXML(Definitions.API_URL + session_encoding, out result);
            var optOutResult = XDocument.Parse(result);
            var value = optOutResult.XPathSelectElements("/Envelope/Body/RESULT/SUCCESS").FirstOrDefault();
            if (value == null) throw new NullReferenceException("No message received from SilverPop");
            if (value.Value.ToLowerInvariant() != "success")
            {
                removeFromListResult.Removed = false;
                var reason = optOutResult.XPathSelectElements("/Envelope/Body/Fault/FaultString").FirstOrDefault();
                if (reason == null)
                {
                    removeFromListResult.Fault = "Unknown error. No FaultString.";
                }
                else
                {
                    removeFromListResult.Fault = reason.Value;
                }
            }
            else
            {
                removeFromListResult.Removed = true;
            }
            return removeFromListResult;
        }

    }

    public class RemoveFromListResult
    {
        public string ListId { get; set; }
        public bool Removed { get; set; }
        public string Fault { get; set; }
    }

    public class SilverPopListForEmail : SilverPopList
    {
        public DateTime? OptOutDate { get; set; }
        public bool? IsInList { get; set; }
    }

    public class SilverPopList
    {
        public string ListId { get; set; }
        public string ListName { get; set; }
    }

    public class SilverPopMailing
    {
        public string MailingId { get; set; }

        public string ListId { get; set; }
        public string ReportId { get; set; }
        public string MailingName { get; set; }
        public DateTime SentTS { get; set; }
        public string TotalOpens { get; set; }
        public string TotalClickstreams { get; set; }
        public string TotalClicks { get; set; }
        public string TotalConversions { get; set; }
        public string TotalAttachments { get; set; }
        public string TotalForwards { get; set; }
        public string TotalMediaPlays { get; set; }
        public string TotalBounces { get; set; }
        public string TotalOptOuts { get; set; }
        public string ListName { get; set; }
        public DateTime? OptOutDate { get; set; }
    }

  

}


