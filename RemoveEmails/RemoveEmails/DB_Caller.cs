﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RemoveEmails
{
    class DB_Caller
    {


        #region "Log Table Calls"

            public static void PurgeLogTable()
            {
                List<RemoveEmailsSilverPop_Logs> logs;

                var old_log_dates =
                    DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["PurgeDaysOlderThan"]));

                SPAPPSEntities db = new SPAPPSEntities();

                try
                {
                    logs = db.RemoveEmailsSilverPop_Logs.Where(a => a.LoggedOnDate < old_log_dates).ToList();
                    db.RemoveEmailsSilverPop_Logs.RemoveRange(logs);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                    //we want to log any errors that occur
                    Logger log = LogManager.GetLogger("databaseLogger");
                    log.Error(ex.InnerException.ToString());
                    Console.WriteLine(ex.InnerException.ToString());

                    // Email
                    Emailer.EmailAdminOfError(ex.ToString(), Utility.GetCurrentMethod());
                }


            }

        #endregion

        #region "Stored Procedure Calls"

        public static Int16 LoadEmailsToBeRemoved()
        {

            ObjectParameter OutputStatus = new System.Data.Entity.Core.Objects.ObjectParameter("status", typeof(int));
            ObjectParameter OutputErrorMessage = new System.Data.Entity.Core.Objects.ObjectParameter("errormessage", typeof(string));
            Int16 status = 0;

            try
            {
                using (var context = new SPAPPSEntities())
                {
                    context.uspRemoveEmailsSilverpop(OutputStatus, OutputErrorMessage);
                }

                
                if (OutputStatus == null) { status = 1; } else { status = Convert.ToInt16(OutputStatus.Value); }
            }
            catch (Exception ex)
            {
                status = 1;

                //we want to log any errors that occur
                Logger log = LogManager.GetLogger("databaseLogger");
                log.Error(ex.InnerException.ToString());
                Console.WriteLine(ex.InnerException.ToString());

                // Email
                Emailer.EmailAdminOfError(ex.ToString(), Utility.GetCurrentMethod());
            }          
           

            return status;

        }

        #endregion

        public static List<RemoveSilverpopEmail> EmailsToBeRemoved()
        {
            SPAPPSEntities db = new SPAPPSEntities();
            List<RemoveSilverpopEmail> Email_List = null;

            try
            {
                Email_List = db.RemoveSilverpopEmails.ToList();
            }
            catch (Exception ex)
            {

                //we want to log any errors that occur
                Logger log = LogManager.GetLogger("databaseLogger");
                log.Error(ex.InnerException.ToString());
                Console.WriteLine(ex.InnerException.ToString());

                // Email
                Emailer.EmailAdminOfError(ex.ToString(), Utility.GetCurrentMethod());
            }
            

            return Email_List;
        }

    }
}
