﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoveEmails
{
    class Emailer
    {
        public static void EmailAdminOfError(string message, string location)
        {
            StringBuilder body = new StringBuilder();

            body.AppendLine("<div style='padding-top:20px;padding-bottom:40px;font-family:Verdana, Geneva, sans-serif;'>");
            body.AppendLine("<p><h5>There was an error in the Import Process.  Details Below</h5></p>");
            body.AppendLine("</div>");

            body.AppendLine("<table style='border: 6px solid #948473;" +
                               "background-color: #FFE3C6;" +
                               "width: 100%;" +
                               "text-align: center;" +
                             "font-family:Verdana, Geneva, sans-serif;'>");
            body.AppendLine("<thead style='background: #948473;" +
                                "background: -moz-linear-gradient(top, #afa396 0%, #9e9081 66%, #948473 100%);" +
                                "background: -webkit-linear-gradient(top, #afa396 0%, #9e9081 66%, #948473 100%);" +
                                "background: linear-gradient(to bottom, #afa396 0%, #9e9081 66%, #948473 100%);" +
                                "'><tr>" +
                                "<th style='border: 1px solid #948473;padding: 4px 4px;font-size: 17px;font-weight: bold;color: #F0F0F0;text-align: left;border-left: 2px solid #948473;border-left: none;'>Error Message</th>" +
                                "</thead></tr>");
            body.AppendLine("<tbody>" + message + "</tbody>");
            body.AppendLine("</table>");

            SPAPPSEntities db = new SPAPPSEntities();

            db.uspRemoveSilverPopEmailAdminError(body.ToString(),
                                            message,
                                            ConfigurationManager.AppSettings["AdminEmail"].ToString(),
                                            "Silverpop Email removal Error at : " + location
                                        );


        }
    }
}
