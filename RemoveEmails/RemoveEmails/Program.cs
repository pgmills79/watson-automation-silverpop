using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoveEmails
{
    class Program
    {
        static void Main(string[] args)
        {
         
            Int16 status = 0; //so far so good, no errors :-)

            //go ahead and purge old log records (It will increase in size quickly)
            DB_Caller.PurgeLogTable();            

            PushMessages("<<<   Begin: Remove Email From SilverPop Process   >>>", true);
            PushMessages("Step #1: Load the Table of Emails to be removed", true);

            //Step #1 Load the Table of Emails to be removed
            status = DB_Caller.LoadEmailsToBeRemoved();

            PushMessages("END Step #1: Load the Table of Emails to be removed", true);
            PushMessages("Step #2: Remove the emails from Silverpop", true);

            #region "Step 2 - Remove Emails SilverPop"

                //Step #2   Remove the emails from Silverpop where the email exists in the table
                //          we loaded in Step #1  
                if (status.Equals(0))
                {
                    List<RemoveSilverpopEmail> email_list = DB_Caller.EmailsToBeRemoved();

                    if (email_list == null)
                    { status = 1; }
                    else
                    {
                        foreach (RemoveSilverpopEmail email in email_list)
                        {

                            List<SilverPopMailing> mailing_lists = API_Handler.EmailHistory(email.Email_Address)
                                                       .Where(m => m.OptOutDate == null).ToList();

                            if (mailing_lists.Count() > 0)
                            {
                                foreach (SilverPopMailing list in mailing_lists)
                                {
                                    //OptOutOfThis List for the user
                                    //API_Handler.OptOutOfList(email.Email_Address, list.MailingId);
                                }
                            }
                        }
                    }
                }

            #endregion

            PushMessages("END Step #2: Remove the emails from Silverpop", true);
            PushMessages("<<<   End: Remove Email From SilverPop Process   >>>", true);
            PushMessages(".......", false);
            PushMessages(".......", false);
            PushMessages("Please Enter Any Key to Exit Program", false);
            Console.ReadKey();            
            
        }

        private static void PushMessages(string message, bool logmessage)
        {
            if (logmessage)
            {//create our logging object for all Donor Import Staging Events
              Logger log = LogManager.GetLogger("databaseLogger");
              log.Info(message);
             }

            Console.WriteLine(message);
        }        
        
    }
}
