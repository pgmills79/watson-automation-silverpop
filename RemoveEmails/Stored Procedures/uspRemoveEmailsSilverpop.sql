USE [SPAPPS]
GO

/****** Object:  StoredProcedure [dbo].[uspRemoveEmailsSilverpop]    Script Date: 10/17/2018 10:40:36 AM ******/
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspRemoveEmailsSilverpop' AND type = 'P')
    DROP PROCEDURE dbo.uspRemoveEmailsSilverpop
GO
GO

/****** Object:  StoredProcedure [dbo].[uspRemoveEmailsSilverpop]    Script Date: 10/17/2018 10:40:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[uspRemoveEmailsSilverpop] 
			@status INT OUTPUT,
			@errormessage AS VARCHAR(MAX) OUTPUT
AS

--auto roll back transaction if there is an error
SET XACT_ABORT ON
--dont want to lock any tables across this SPROC
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET NOCOUNT ON


BEGIN TRY
        
        SET @status = 0  --zero means no error
		 
        --log the beginning of the SPROC (so we can keep track of how long things run)
        INSERT INTO dbo.RemoveEmailsSilverPop_Logs
		(
			Level,
			CallSite,
			Type,
			Message,
			StackTrace,
			InnerException,
			AdditionalInfo,
			LoggedOnDate
		)    
		VALUES 
		(
			'Info',
			'uspRemoveEmailsSilverpop',
			'', 
			'<<< BEGIN PROCESS: Adding Emails to RemoveSilverpopEmails Table to Be Removed >>>',
			'', 
			'',
			'',
			GETDATE()
		)	

	
		--first clear out table 
		TRUNCATE TABLE dbo.RemoveSilverpopEmails


		--then add all the emails from donor to be prepped for removal
		--from SilverPop Email lists
		INSERT INTO dbo.RemoveSilverpopEmails
		(
			Account_Number,
			Email_Address,
			Date_Removed
		)
		SELECT email.AccountNumber, email.EmailAddress, NULL 
		FROM SPDSPROD.dbo.A01_AccountMaster am
		INNER JOIN SPDSPROD.dbo.A01cAccountCodes ac
			ON ac.AccountNumber = am.AccountNumber
			AND ac.CodeValue IN ('130','111')
			AND ac.CodeType = 'SPECIAL'
			AND ac.Active = 1
		INNER JOIN SPDSPROD.dbo.A07_AccountEmails email
			ON email.AccountNumber = am.AccountNumber
		WHERE am.Status = 'A'
		GROUP BY email.AccountNumber, email.EmailAddress
		ORDER BY email.AccountNumber

		DECLARE @number_records VARCHAR(MAX)

		SET @number_records = CAST((SELECT COUNT(*) FROM dbo.RemoveSilverpopEmails) AS VARCHAR)

		INSERT INTO dbo.RemoveEmailsSilverPop_Logs
		(
			Level,
			CallSite,
			Type,
			Message,
			StackTrace,
			InnerException,
			AdditionalInfo,
			LoggedOnDate
		)    
		VALUES 
		(
			'Info',
			'uspRemoveEmailsSilverpop',
			'', 
			'Emails have been loaded and ready to be removed (OPTED OUT of emails) from SilverPop.  Total Emails Loaded: ' + @number_records,
			'', 
			'',
			'',
			GETDATE()
		)
		
		  --log the beginning of the SPROC (so we can keep track of how long things run)
        INSERT INTO dbo.RemoveEmailsSilverPop_Logs
		(
			Level,
			CallSite,
			Type,
			Message,
			StackTrace,
			InnerException,
			AdditionalInfo,
			LoggedOnDate
		)    
		VALUES 
		(
			'Info',
			'uspRemoveEmailsSilverpop',
			'', 
			'<<< END PROCESS: Adding Emails to RemoveSilverpopEmails Table to Be Removed >>>',
			'', 
			'',
			'',
			GETDATE()
		)		


END TRY

BEGIN CATCH

	SET @status = 1

	SELECT @errormessage = SUBSTRING('ERROR IN  = uspRemoveEmailsSilverpop, PROCEDURE LINE ' + 
								   RTRIM(CONVERT(CHAR(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255)
								   
	--log the error message in our importing process
    INSERT INTO dbo.RemoveEmailsSilverPop_Logs
    (
        Level,
        CallSite,
        Type,
        Message,
        StackTrace,
        InnerException,
        AdditionalInfo,
        LoggedOnDate
    )    
	VALUES 
	(
		'Error',
		'uspRemoveEmailsSilverpop',
		'', 
		'',
		'', 
		ERROR_MESSAGE(),
		@errormessage,
		GETDATE()
	)		
								    
END CATCH
GO
