USE [SPAPPS]
GO

/****** Object:  StoredProcedure [dbo].[uspRemoveSilverPopEmailAdminError]    Script Date: 4/5/2018 9:37:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspRemoveSilverPopEmailAdminError' AND type = 'P')
    DROP PROCEDURE uspRemoveSilverPopEmailAdminError
GO

CREATE PROCEDURE [dbo].[uspRemoveSilverPopEmailAdminError]
		@BODY VARCHAR(500),
		@ERRORMESSAGE VARCHAR(500),
		@EMAIL_ADDRESS VARCHAR(50),
		@SUBJECT VARCHAR(100)
AS
SET NOCOUNT ON

 EXEC msdb.dbo.sp_send_dbmail 
 @recipients = @EMAIL_ADDRESS, 
 @SUBJECT = @SUBJECT, 
 @BODY = @BODY,
 @BODY_FORMAT = 'HTML'

GO