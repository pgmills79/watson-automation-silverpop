USE [SPAPPS]
GO

/****** Object:  StoredProcedure [dbo].[uspRemoveSilverPopEmailsInsertLog]    Script Date: 10/17/2018 10:40:36 AM ******/
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspRemoveSilverPopEmailsInsertLog' AND type = 'P')
    DROP PROCEDURE dbo.uspRemoveSilverPopEmailsInsertLog
GO
GO

/****** Object:  StoredProcedure [dbo].[uspRemoveSilverPopEmailsInsertLog]    Script Date: 10/17/2018 10:40:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[uspRemoveSilverPopEmailsInsertLog] 
(
	@level VARCHAR(MAX),
	@callSite VARCHAR(MAX),
	@type VARCHAR(MAX),
	@message VARCHAR(MAX),
	@stackTrace VARCHAR(MAX),
	@innerException VARCHAR(MAX),
	@additionalInfo VARCHAR(MAX)
)
AS

INSERT INTO dbo.RemoveEmailsSilverPop_Logs
(
    Level,
    CallSite,
	Type,
    Message,
    StackTrace,
    InnerException,
    AdditionalInfo
)
VALUES
(
	@level,
	@callSite,
	@type,
	@message,
	@stackTrace,
	@innerException,
	@additionalInfo
)

GO


