USE [SPAPPS]
GO

IF OBJECT_ID('dbo.RemoveEmailsSilverPop_Logs', 'U') IS NOT NULL 
  DROP TABLE dbo.RemoveEmailsSilverPop_Logs; 

/****** Object:  Table [dbo].[RemoveEmailsSilverPop_Logs]    Script Date: 2/13/2019 9:44:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RemoveEmailsSilverPop_Logs](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[Level] [varchar](max) NOT NULL,
	[CallSite] [varchar](max) NOT NULL,
	[Type] [varchar](max) NOT NULL,
	[Message] [varchar](max) NOT NULL,
	[StackTrace] [varchar](max) NOT NULL,
	[InnerException] [varchar](max) NOT NULL,
	[AdditionalInfo] [varchar](max) NOT NULL,
	[LoggedOnDate] [datetime] NOT NULL,
 CONSTRAINT [pk_RemoveEmailsSilverPop_Logs] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[RemoveEmailsSilverPop_Logs] ADD  CONSTRAINT [df_remove_emailssilverpop_logs_loggedondate]  DEFAULT (getdate()) FOR [LoggedOnDate]
GO