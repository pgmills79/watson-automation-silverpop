USE [SPAPPS]
GO

/****** Object:  Table [dbo].[RemoveSilverpopEmails]    Script Date: 2/13/2019 10:14:27 AM ******/
IF OBJECT_ID('dbo.RemoveSilverpopEmails', 'U') IS NOT NULL 
  DROP TABLE dbo.RemoveSilverpopEmails

/****** Object:  Table [dbo].[RemoveSilverpopEmails]    Script Date: 2/13/2019 10:14:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RemoveSilverpopEmails](
	[ID] [BIGINT] IDENTITY(1,1) NOT NULL,
	[Account_Number] [BIGINT] NULL,
	[Email_Address] [VARCHAR](MAX) NULL,
	[Date_Removed] [DATETIME] NULL,
 CONSTRAINT [PK_RemoveSilverpopEmails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


